import package1.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle firstCircle = new Circle();
        Circle secondCircle = new Circle(3.0);



        // Hiển thị diện tích và chu vi hình tròn

        System.out.print("Area: " + firstCircle.getArea() + "\n");
        System.out.print("Circumference: " + firstCircle.getCircumference() + "\n");

        System.out.print("Area: " + secondCircle.getArea() + "\n");
        System.out.print("Circumference: " + secondCircle.getCircumference() + "\n");
    }
}
